package com.example.demologin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthOptions;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

public class phoneVerificationActivity extends AppCompatActivity {

    EditText phoneNo;
    Button send;
    private FirebaseAuth auth;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_verification);

        phoneNo = findViewById(R.id.etPhone);
        send = findViewById(R.id.btnSend);
        progressBar = findViewById(R.id.progressBar);
        auth = FirebaseAuth.getInstance();


        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (phoneNo.getText().toString().trim().isEmpty()){
                    Toast.makeText(phoneVerificationActivity.this, "Invalid Phone Number", Toast.LENGTH_SHORT).show();
                }
                else if(phoneNo.getText().toString().trim().length() != 10){
                    Toast.makeText(phoneVerificationActivity.this, "Type Valid Phone Number", Toast.LENGTH_SHORT).show();
                }
                else {
                    otpSend();
                }
            }
        });
    }

    public  void otpSend(){
        progressBar.setVisibility(View.VISIBLE);
        send.setVisibility(View.GONE);
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {

            }

            @Override
            public void onVerificationFailed(FirebaseException e) {

                progressBar.setVisibility(View.GONE);
                send.setVisibility(View.VISIBLE);
                Toast.makeText(phoneVerificationActivity.this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCodeSent(@NonNull String verificationId,
                                   @NonNull PhoneAuthProvider.ForceResendingToken token) {

                progressBar.setVisibility(View.GONE);
                send.setVisibility(View.VISIBLE);
                Toast.makeText(phoneVerificationActivity.this, "OTP Send Successfully", Toast.LENGTH_SHORT).show();
                Intent i =new Intent(phoneVerificationActivity.this,otpVerificationActivity.class);
                i.putExtra("phone",phoneNo.getText().toString().trim());
                i.putExtra("verificatioId",verificationId);
                startActivity(i);
                finish();

            }
        };

        PhoneAuthOptions options =
                PhoneAuthOptions.newBuilder(auth)
                        .setPhoneNumber("+91"+phoneNo.getText().toString().trim())
                        .setTimeout(60L, TimeUnit.SECONDS)
                        .setActivity(this)
                        .setCallbacks(mCallbacks)
                        .build();
        PhoneAuthProvider.verifyPhoneNumber(options);
    }



}