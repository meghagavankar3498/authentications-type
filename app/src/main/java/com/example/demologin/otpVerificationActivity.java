package com.example.demologin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

public class otpVerificationActivity extends AppCompatActivity {

    private  String verificationId;
    EditText et1,et2,et3,et4,et5,et6 ;
    TextView phone , resend;
    Button verify;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verification);

        phone = findViewById(R.id.tvMobile);
        resend = findViewById(R.id.tvResendBtn);
        verify = findViewById(R.id.btnVerify);
        et1 =findViewById(R.id.etC1);
        et2 =findViewById(R.id.etC2);
        et3 =findViewById(R.id.etC3);
        et4 =findViewById(R.id.etC4);
        et5 =findViewById(R.id.etC5);
        et6 =findViewById(R.id.etC6);
        progressBar = findViewById(R.id.progressBarVerify);

        editTextInput();
        phone.setText(String.format("+91-%s",getIntent().getStringExtra("phone")));
        verificationId = getIntent().getStringExtra("verificatioId");

        resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(otpVerificationActivity.this, "OTP Send Successfully", Toast.LENGTH_SHORT).show();
            }
        });

        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);
                verify.setVisibility(View.INVISIBLE);
                if (et1.getText().toString().trim().isEmpty()|| et2.getText().toString().trim().isEmpty() ||
                        et3.getText().toString().trim().isEmpty() ||
                        et4.getText().toString().trim().isEmpty() ||
                        et5.getText().toString().trim().isEmpty() ||
                        et6.getText().toString().trim().isEmpty()){
                    Toast.makeText(otpVerificationActivity.this, "OTP is NOT Valid", Toast.LENGTH_SHORT).show();
                }
                else {
                    if ( verificationId != null){
                        requestPermission();
                        String code = et1.getText().toString().trim() +
                                et2.getText().toString().trim() +
                                et3.getText().toString().trim() +
                                et4.getText().toString().trim() +
                                et5.getText().toString().trim() +
                                et6.getText().toString().trim();
                        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
                        FirebaseAuth.getInstance().signInWithCredential(credential)
                                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        if (task.isSuccessful()){
                                            progressBar.setVisibility(View.GONE);
                                            verify.setVisibility(View.INVISIBLE);
                                            Toast.makeText(otpVerificationActivity.this, "OTP Verified", Toast.LENGTH_SHORT).show();
                                            Intent i = new Intent(otpVerificationActivity.this,HomeActivity.class);
                                            startActivity(i);
                                            finish();
                                        }
                                        else {
                                            progressBar.setVisibility(View.GONE);
                                            verify.setVisibility(View.INVISIBLE);
                                            Toast.makeText(otpVerificationActivity.this, "OTP is NOT Valid", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                    }
                }
            }
        });
    }

    private void requestPermission() {

        if (ContextCompat.checkSelfPermission(otpVerificationActivity.this, Manifest.permission.RECEIVE_SMS)
                != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(otpVerificationActivity.this,new String[]{
                    Manifest.permission.RECEIVE_SMS
            },100);
        }
    }

    private void editTextInput() {
        et1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                et2.requestFocus();

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        et2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                et3.requestFocus();

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        et3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                et4.requestFocus();

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        et4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                et5.requestFocus();

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        et5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                et6.requestFocus();

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }
}